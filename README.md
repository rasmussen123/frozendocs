![worldoftanks.com](https://worldoftanks.com/dcont/fb/news/arctic_exploration_weekend/arctic_exploration_weekend_684.jpg?MEDIA_PREFIX=/dcont/fb/)

# Project FrozenFist

## VortexOps


>The project, codenamed **"FrozenFist"**, is a software design project for semi-autonomous armored assault vehicles. The vehicles
>are designed to operate either remotely with a human pilot or in *"robot mode"* with guidance from an on-board Artificial
>Intelligence (AI) system.


Contact: [Holly Pinkston](holly.pinkston@smail.rasmussen.edu)

*** Pushed to BitBucket repository - 05/26/18

### Modified README on BitBucket - 05/27/18
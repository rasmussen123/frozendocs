# To Do List for BZFlag:


### THESE SHOULD HAPPEN BEFORE THE NEXT RELEASE:

1. Name the version "*Psyche*".

2. Ship it.

3. Notify all those listed in the end of the README.

### THESE ARE LOWER PRIORITY:

1. **Need to use different, clearly disjoint numbering system on bzfs.**
  - Perhaps just start from 0001 when 1.10 is released.  Otherwise, it gets 
  unnecessarily bumped to match the client version.  It's a separate 
  communication protocol version.

2. Server should track last reported player and shot positions.

3. **Improve server picked spawn locations.**
  - Server needs to track more game state to do this well.

4. **Remove UDP client setting.**
  - Code should figure that out now.

5. **Add other DB server features.**
  * see [http://bzflag.org/wiki/DbServers](http://bzflag.org/wiki/DbServers);
  * player/password registration;
  * open group registration; (One nick is master for each group.)
  * karma see [http://bzflag.org/wiki/KarmaSystem](http://bzflag.org/wiki/KarmaSystem).

6. **Try to build bzadmin on Win32 using PDCurses.**
  - (It works with the X11 version of PDCurses.) It currently works with stdin/stdout.

7. Add ability to capture a screenshot to a gamma-corrected png from
  inside the game.

8. Rewrite Mac OS X display/window code to support app switching,
  resolution mode changes, screen captures, etc.

9. Make it so keys can be bound to multiple actions, and provide a way
  to do global scoping on key actions.

10. StateDatabase should probably have min max values for eval values.

11. Split -public <desc> into -public, -description <desc>, and -email <contact>.

12. **Support GameInfo requests as one time UDP packets and include all the
  same information that bzfls tracks.**
    - We should allow server info, player and score lists over udp from any udp
    address, not just a "*connected*" player.

13. **Make bCast traffic and serverlist traffic less blocking.**
  - *(ie: same select() call)*

14. Listen all the time on UDP for bCast game info replies when in Find Servers.

15. **Would be nice if LocalPlayer<->Robots wouldn't relayPackets
  (MsgPlayerUpdate) through server.**
    - BZflag should be able to act as a relay for a local player and the server 
    should know to only send one MsgUpdate to the master, who will relay to the
    connected players/bots. This will allow multiple players behind one slow net
    connection to play multiple players behind another slow connection. 
    *(for example) ie: -relay*

16. **BZflag -relay should attempt to listen and reply on udp, resending server
  info for which ever server it is connected to.**
    - Descriptions should begin with "relay:". In this case, -solo <n> should 
    enable this behavior.

17. **Rework the BZDB->eval() caching to support expression
  dependencies.**
    - If a variable changes, all the expressions 
    that use that variable should be updated as well, or at 
    the least, flush the entire cache when a callback happens.

18. **Make the vertical rico solution more elegant.**
    - Get rid of BoxBuilding::getNormal and fix existing code to do z - 
    getNormalRect & the like.

19. **Require an email contact name for public servers.**
    - Perhaps unpublished by default

20. **Create a new MsgThrottle message sent from client to server
  to inform server to throttle MsgPlayerUpdates from other clients
  to X.**
    - X is set in bzflag.bzc. Server uses PlayerInfo.lastState to 
    batch send PlayerUpdates after throttle time has passed. 
    Clients timestamp updates themselves, to catch out of order 
    packets, but server restamps timestamps to get consistent 
    times for all messages.

21. Lag information should be appended to MsgPlayerUpdate packet
  by server, and use half in dead reckoning calculations.

22. Remove all DNS lookups from client when contacting a server IP
  supplied from the list server.

23. Add http proxy support for list servers.

24. Add http connect proxy support for game servers.

25. **Allow /dev/dsp* on Linux to be selected someplace.**
    - command line, environment var, config file... ***Who knows? ;-)***

26. **Some bzdb values are stored elsewhere and written to bzdb
  only on exit.**
    - These should be stored in bzdb for the entire time.

27. **Add caching to bzdb for integer/float values, so they don't
  have to be atoi()'ed or eval()'ed all the time.**
    - isTrue would also be a good one to cache.

28. Document headers with JavaDoc-style comments (for doxygen).

29. Update doc/protocol.txt all descriptors to new format.

30. Support gameinfo requests as one time UDP packets (if UDP).

31. **BZAdmin should build without GL headers installed.**
    - Fails with: ../../include/bzfgl.h:35: GL/gl.h. 
    *No such file or directory.*

32. Build a prototype(4) for BSD, *Solaris*.

33. Fix up irix IDB.

34. If we stay with TCP/UDP, then use the same ports for the
   UDP pipe as for the TCP pipe on both client and server.

35. **Encapsulate stuff requiring platform #ifdef's:**
   * networking API into libNet.a.;
   * fork/exec (used in menus.cxx) into libPlatform.a;
   * file system stuff (dir delimiter, etc.)
   * user name stuff

36. Clean up libraries that could be reused by other games.

37. **Move robots to separate binaries and allow either the
   client or server to exec them.**
     - Have a server option to keep n players in the game which 
     auto spawns/kills enough bots to do it. Get rid of bot player 
     type completely. Bots should get all message types.

38. Smarter robots

39. Add type of shot (*normal, gm, sw, etc*) to killed message.

40. **Radio chat:**
     - Allow players to communicate via low-quality audio. 
     (Already some support built into the server for this.)

### THESE NEED TO WAIT FOR THE NEXT PROTOCOL BREAKAGE:

1. **Move -synctime data to different Msg packet, or create new one.**
    - MsgSetVar (ala 1.8) would be a good one.

2. **Flags should all be nullflags from client view *until*:**
  a) you pick one up; ***OR***
  b) you get the identify flag. 
    - (Send updates for all flags?) If player drops a flag and it stays, 
    players have it's real ID.

3. **Shorten laser to like 1.25 normal shot.**
    - This is needed for larger maps so we can send sparse player updates.

4. Server should send less frequent updates for "*distant*" players.

5. Move team base packing into WorldInfo (bzfs) and add height (z).

6. Remove *flipz* from pyr net code and replace with neg height.

7. Remove *shoothrough/drivethrough* and replace with one flag,
  meaning passable.

8. IPV6 support

9. Consider using [ENet](http://enet.cubik.org/) for
   the network protocol.

10. Implement texture cache on client and have server offer
   new textures as part of the world.

11. Allow loadable meshes as "*tanks*".

12. Supply tank "*meshes*" from the server.

13. Allow client to choose a mesh from the server list.

14. Implement one way cross server teleporters.

15. Implement a visual "*server list*" game server.

16. **Pre-game waiting room:**
   * Provide a means for players to gather before and after
   a game;* basic text chat facilities;* allows players
   to wait for enough players for a game and to discuss
   strategy, etc.  
     - (Could build this into bzfs easily.)
